import React, { useState, useEffect } from 'react';

function Menu({levels, handleSelectLevel}) {
  return (
    <header className="navbar navbar-expand navbar-dark bg-dark">
      <a className="navbar-brand" href="#">
        Code4Kids / Уровень:
      </a>

      <div className="nes-select is-dark">
        <select onChange={ handleSelectLevel }>
          <option value="" disabled selected hidden>Выбери уровень...</option>
          { levels.map(level => <option value={ level.id } key={ "level-id-" + level.id }>{ level.name }</option>) }
        </select>
      </div>
      
      <a href="https://gitlab.com/grigoriytretyakov/codeforkids/-/blob/main/README.md" target="_blank" className="nes-btn is-warning">Документация</a>
    </header>
  )
}

export default Menu;
