import React from 'react'
import ReactMarkdown from 'react-markdown'


function LevelDescription({ level }) {
    if (!level) {
        level = {
            name: 'Привет',
            description: 'Добро пожаловать. Выбери уровень, который хочешь пройти.'
        }
    }
    return (
        <>
            <div className="nes-container with-title">
                <h3>{level.name}</h3>
                <ReactMarkdown>{level.description}</ReactMarkdown>
            </div>
        </>
    )
}

export default LevelDescription;    