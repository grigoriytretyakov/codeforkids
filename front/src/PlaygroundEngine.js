
export class PlaygroundEngine {
    constructor() {
        this.fieldData = null
        this.steps = []
        this.field = []
        this.items = []
        this.changelog = 0
        this.currentStep = 0
    }

    setFieldData(fieldData) {
        this.fieldData = fieldData

        this.reload()
    }

    setSteps(steps) {
        this.steps = steps

        this.reload()
    }

    reload() {
        this.field = []
        this.items = []

        for (let i in this.fieldData) {
            const line = []
            for (let j in this.fieldData[i]) {
                line.push(this.fieldData[i][j])
            }
            this.field.push(line)
        }

        this.currentStep = 0
        this.changelog++
    }

    hasStep() {
        return this.steps && this.currentStep < this.steps.length
    }

    makeStep() {
        let moves = {}
        for (let i in this.steps[this.currentStep]) {
            let event = this.steps[this.currentStep][i]
            if (event.action === 'move') {
                moves[event.src] = event.dst
            }
        }
        let nextField = []
        for (let i in this.field) {
            const line = []
            for (let j in this.field[i]) {
                line.push('.')
            }
            nextField.push(line)
        }
        for (let i in this.field) {
            for (let j in this.field[i]) {
                if (this.field[i][j] != '.') {
                    if (moves[[i, j]]) {
                        const [nl, nc] = moves[[i, j]]
                        nextField[nl][nc] = this.field[i][j]
                    }
                    else {
                        if (nextField[i][j] === '.') {
                            nextField[i][j] = this.field[i][j]
                        }
                    }
                }
            }
        }
        this.field = nextField
        this.currentStep++
        this.changelog++
    }

    isSuccessful() {
        return this.steps[this.steps.length - 1][0].status === 'win'
    }
}