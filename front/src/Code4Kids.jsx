import React, { useState, useEffect } from 'react';

import './Code4Kids.css';

import Menu from './Menu';
import LevelDescription from './LevelDescription';
import CodeEditor from './CodeEditor';
import GameView from './GameView';


function Code4Kids() {
  const [levels, setLevels] = useState([]);
  const [activeLevel, setActiveLevel] = useState(null);
  const [solution, setSolution] = useState(null);

  useEffect(() => {
    fetch('/api/levels')
      .then(response => response.json())
      .then(json => {
        setLevels(json)
      })
      .catch(error => console.error(error));
  }, []);

  function handleSelectLevel(e) {
    setSolution(null)

    fetch('/api/levels/' + e.target.value)
      .then(response => response.json())
      .then(json => {
        setActiveLevel(json)
      })
      .catch(error => console.error(error));
  }

  function handlePost(code) {
    let options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' , 'accept': 'application/json'},
      body: JSON.stringify({
        level_id: activeLevel.id,
        code: code
        })
    }
    fetch('/api/levels/' + activeLevel.id, options)
      .then(response => response.json())
      .then(json => {
        setSolution({
          timestamp: json.timestamp,
          steps: JSON.parse(json.steps)
        })
      })
  }


  return (
    <>
      <Menu levels={levels} handleSelectLevel={handleSelectLevel} />

      <div className="container-fluid">
        <div className='row'>
          {!activeLevel ? (
            <>
              <div className="col-md-3"></div>

              <div className="col-md-6">
                <section className="nes-container is-dark">
                  <section className="message-list">

                    <section className="message -left">
                      <div className="nes-balloon from-left is-dark">
                        <h1>Чтобы начать, выбери уровень.</h1>
                      </div>
                      <i className="nes-bcrikko"></i>
                    </section>

                  </section>
                </section>
              </div>

              <div className="col-md-3"></div>
            </>

          ) : (
            <>
              <div className='col-md-4'>
                <LevelDescription level={activeLevel} />
              </div>

              <div className='col-md-8'>
                <div className='row'>
                  <div className='col-sm-12 col-md-5'>
                    <CodeEditor level={activeLevel} handlePost={handlePost} />
                  </div>
                  <div className='col-sm-12 col-md-7'>
                    <GameView 
                    level={activeLevel} 
                    solution={solution} 
                    />
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>

  )
}

export default Code4Kids;
