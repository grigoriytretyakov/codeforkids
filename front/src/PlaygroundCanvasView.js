const DEFAULT_BACKGROUND = '#999999'

const CELL_SIZE = 32;


function drawBasicView(canvas, field) {
    const colorMap = {
        '.': '#aaaaaa',
        'h': '#28a745',
        'w': '#333333',
        'b': '#209cee',
        'e': '#f7d51d',
        's': '#aa2222'
    };

    const context = canvas.getContext('2d');

    context.fillStyle = DEFAULT_BACKGROUND;
    context.fillRect(0, 0, canvas.width, canvas.height);

    for (let l in field) { 
        for (let c in field[l]) {
            context.fillStyle = colorMap[field[l][c]];
            let x = c * CELL_SIZE;
            let y = l * CELL_SIZE;
            context.fillRect(x, y, CELL_SIZE, CELL_SIZE)
        }
    }
}


function drawImagesView(canvas, field, images) {
    const context = canvas.getContext('2d');

    context.fillStyle = DEFAULT_BACKGROUND;
    context.fillRect(0, 0, canvas.width, canvas.height);

    for (let l in field) { 
        for (let c in field[l]) {
            let x = c * CELL_SIZE;
            let y = l * CELL_SIZE;
            context.drawImage(images['.'], x, y) // фоновую клетку нужно нарисовать в любом случае
            context.drawImage(images[field[l][c]], x, y)
        }
    }
}


export function drawPlayground(canvas, field, images) {
    if (! images) {
        drawBasicView(canvas, field)
    }
    else {
        drawImagesView(canvas, field, images)
    }
}