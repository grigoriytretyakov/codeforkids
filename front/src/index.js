import React from 'react';
import ReactDOM from 'react-dom/client';

import 'bootstrap/dist/css/bootstrap.css';
import 'nes.css/css/nes.min.css';
import './index.css';

import Code4Kids from './Code4Kids';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Code4Kids />
  </React.StrictMode>
);
