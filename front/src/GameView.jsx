import { useEffect, useRef, useState } from "react";
import { drawPlayground } from "./PlaygroundCanvasView";
import { PlaygroundEngine } from "./PlaygroundEngine";

import empty from './pics/empty.png'
import hero from './pics/hero.png'
import box from './pics/box.png'
import exit from './pics/exit.png'
import wall from './pics/wall.png'
import spike from './pics/spike.png'


function GameView({ level, solution }) {
    const canvasRef = useRef(null)
    const [playgroundChanged, setPlagroundChanged] = useState(null)
    const [finishMsg, setFinishMsg] = useState('')
    const [playground] = useState(new PlaygroundEngine())

    const [images, setImages] = useState(null)

    useEffect(() => {
        const pairs = {
            '.': empty,
            'h': hero,
            'w': wall,
            'b': box,
            'e': exit,
            's': spike
        }
        let full = {}
        Object.keys(pairs).forEach((k) => {
            let i = new Image()
            i.src = pairs[k]
            full[k] = i
        })
        setImages(full)
    }, [])

    function replayPlayground() {
        playground.reload()
        setPlagroundChanged(playground.changelog)
    }

    function updateField() {
        if (! solution) {
            return
        }
        
        if (! playground.hasStep()) {
            return
        }

        playground.makeStep()

        if (! playground.hasStep()) {
            if (playground.isSuccessful()) {
                setFinishMsg('Здорово! Ты выполнил задание. Переходи к следующему уровню.')
            }
            else {
                setFinishMsg('В этот раз у тебя не получилось достичь цели. Попробуй ещё раз.')
            }
        }
        setPlagroundChanged(playground.changelog)
    }

    useEffect(() => {
        playground.setFieldData(level.field.split('\n'))

        if (solution) {
            playground.setSteps(solution.steps)
        }

        setFinishMsg('')
        setPlagroundChanged(playground.changelog)
    }, [level, solution])

    useEffect(() => {
        const DELAY = 500 // Сколько раз в секунду перерисовывать поле
        const intervalId = setInterval(updateField, DELAY)

        return () => { clearInterval(intervalId) }
    }, [solution, playgroundChanged])

    useEffect(() => {
        drawPlayground(canvasRef.current, playground.field, images)
    }, [playgroundChanged])

    return (
        <>

            <canvas width="640" height="640" id="gameview-canvas" ref={canvasRef}></canvas>

            <button
                        type="button"
                        className="nes-btn is-primary btn-block"
                        onClick={() => replayPlayground()}
                    >Повторить</button>

            <p>{finishMsg}</p>


        </>
    )
}

export default GameView;