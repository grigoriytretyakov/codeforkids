import React, { useState, useRef, useEffect } from 'react';

function CodeEditor({ level, handlePost }) {
    const spaces = 4

    const [code, setCode] = useState('');
    const [caret, setCaret] = useState(-1)

    const textAreaRef = useRef(null)

    useEffect(() => {
        if (caret >= 0)
            textAreaRef.current.setSelectionRange(caret + spaces, caret + spaces)
    }, [code])

    function handleTab(e) {
        if(e.key === 'Tab') {
            e.preventDefault();
            let content = e.target.value;
            const { selectionStart, selectionEnd } = e.target;
            let text = content.substring(0, selectionStart) + ' '.repeat(spaces) + content.substring(selectionEnd);

            setCode(text);
            setCaret(selectionStart)
        }
    }

    function handleType(e) {
        setCaret(-1)
        setCode(e.target.value)
    }

    function handleButtonClick(e) {
        handlePost(code)
    }

    return (
        <>
            <textarea
                ref={textAreaRef}
                id="code-solution"
                className="nes-textarea"
                rows="26"
                onChange={handleType}
                onKeyDown={handleTab}
                value={code}
            ></textarea>
            <button
                type="button"
                className="nes-btn is-primary btn-block"
                onClick={handleButtonClick}>Проверить решение!</button>

        </>
    )
}

export default CodeEditor;