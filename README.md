# Code for kids - обучение детей программированию

## Оглавление

- [Введение](#введение)
- [Руководство учителя и разработчика](#руководство-учителя-и-разработчика)
    - [Установка](#установка)
- [Ссылки](#ссылки)

## Введение

Этот проект задуман как подспорье для тех, кто хотел бы помочь детям научиться программировать на python, но не имеет для этого подходящих инструментов. Есть немало интересных площадок с задачами по программированию для детей. Они дают задания в игровом мире. Для решения заданий нужно писать код. Но на этих площадках сложно перейти от решения игровых задач к реальной разработке.

В этом проекте предполагается активное участие учителя (родителя), который будет наблюдать за прогрессом ученика, придумывать для него новые уровни, которые помогут отработать те навыки, которые нужно. **Но самое важное качество этого проекта - расширяемость самой игры.**. Т.е. по мере решения задач, можно (и нужно!) добавлять в игру новые функции. Так по просьбе ребёнка в игре появилась функция прыжка `jump` да ещё и с возможностью вызывать её другим словом (`dragon`). 

Разработку новых функций игры учителю стоит делать вместе с учеником: показывать ему код, объяснять механику действий. А финалом работы с этим проектом может стать добавление какой-то новой возможности для игры (или героя) самим учеником. Также не стоит считать, что Code4Kids - это всё, что нужно для обучения. Это лишь один из инструментов, который может дополнить обучение. Не стоит отказываться от других платформ, а также может быть полезно прост открыть текстовый редактор, написать 10 строк кода на питоне для рисования цветных геометрических фигур, и дать этот код для опытов ребёнку.

Поскольку цель проекта была в том, чтобы начать с чего-то простого, я сделал код настолько простым, насколько мог. Использование классов свёл к минимуму. Тоже самое касается и кода фронтенда.

По сути, для работы с каждым учеником стоит делать бранч под него и в бранче работать с ним отдельно. Поэтому мастер в этом репозитории будет меняться очень медленно, потому что большое количество уровней превратит этот проект в конкурента других платформ, что мне не по плечу :) 

## Руководство учителя и разработчика

### Установка и запуск

Предполагается, что в системе установлен Python.

```
git clone https://gitlab.com/grigoriytretyakov/codeforkids.git
pip install -r backend/requirements.txt
cd backend
fastapi run main.py
```

Открываем http://localhost:8000 (или айпишник отдельного сервера, если у вас это так будет).

### Бэкенд

Код бэкенда лежит в backend. В качестве фреймворка для сервера я взял fastapi и всё серверное приложение положил в один файл main.py.

Игоровая логика и уровни лежат в папке backend/game.

#### Как создать уровень

Чтобы создать новый уровень, нужно зайти в папку backend/games/levels и добавить новую папку. В папке обязательно должны быть файлы:

- level.json - в нём лежит только имя уровня (если честно, до сих пор сомневаюсь, что файл нужен). Структура файла - `{ "name": "Лабиринт" }`
- description.md - текст с описанием и подсказками в Markdown
- map.txt - карта уровня. Это текст 20х20, каждый символ - ячейка на карте.

Символы для карты:

- . - пустота
- w - стена
- s - скелет
- b - ящик
- e - выход

Виды ячеек можно добавлять, но нужно будет внести изменения в код как бэкенда (game.py), так и фронтенда (GameView.jsx, PlaygroundCanvasView.js).

Также там может быть файл level_validation.py, в котором должна быть функция `check_level`. Эта функция даёт возможность сделать любую проверку на победу, а не только совпадение координат героя и выхода.

### Фронтенд

Фронтенд написан с использованием ReactJS. Стили - NES.CSS + Bootstrap. После переделки фронтенда нужно сделать билд: `npm run build`


## Ссылки

- Арт https://opengameart.org/content/simple-tileset
- NES.CSS - https://github.com/nostalgic-css/NES.css