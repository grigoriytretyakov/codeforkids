import json
import os
import time

from typing import Annotated
from fastapi import Depends, FastAPI
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
from sqlmodel import SQLModel, Field, Session, create_engine, select
from starlette.responses import FileResponse

from game.codeplayer import play_code


LEVELS_DIR = 'game/levels/'


class Level(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    path: str = Field()  # unique=True?
    name: str = Field()


class FullLevel(SQLModel):
    id: int
    name: str
    description: str
    field: str


class Solution(BaseModel):
    level_id: int
    code: str

class Result(BaseModel):
    timestamp: float
    level_id: int
    status: str
    steps: str


SQLITE_FILE_NAME = "database.db"
SQLITE_URL = f"sqlite:///{SQLITE_FILE_NAME}"

CONNECT_ARGS = {"check_same_thread": False}

ENGINE = create_engine(SQLITE_URL, connect_args=CONNECT_ARGS)


def create_db():
    SQLModel.metadata.create_all(ENGINE)


def get_session():
    with Session(ENGINE) as session:
        yield session

SessionDep = Annotated[Session, Depends(get_session)]

c4k = FastAPI()

c4k.mount('/static', StaticFiles(directory='static'), name='static')


def fill_db():
    with Session(ENGINE) as session:
        for item in sorted(os.listdir(LEVELS_DIR)):
            full = os.path.join(LEVELS_DIR, item)
            if os.path.isdir(full):
                exist = session.exec(select(Level).where(Level.path == full)).first()
                if exist:
                    continue

                with open(os.path.join(full, 'level.json')) as fp:
                    level_data = json.loads(fp.read())
                with open(os.path.join(full, 'map.txt')) as fp:
                    field = fp.read()
                level = Level(
                    path=full, 
                    name=level_data['name']
                )
                session.add(level)
                session.commit()


@c4k.on_event('startup')
def on_startup():
    create_db()
    fill_db()


@c4k.get('/')
def read_index():
    return FileResponse('templates/index.html')

@c4k.get('/gameview')
def read_gameview():
    return FileResponse('templates/gameview.html')

@c4k.get('/api/levels')
def read_levels(session: SessionDep):
    levels = session.exec(select(Level)).all()
    return levels


@c4k.get('/api/levels/{level_id}', response_model=FullLevel)
def read_level(level_id: int, session: SessionDep):
    level = session.get(Level, level_id)

    with open(os.path.join(level.path, 'description.md')) as fp:
        description = fp.read()

    with open(os.path.join(level.path, 'map.txt')) as fp:
        field = fp.read()

    return FullLevel(
        id=level.id, 
        name=level.name, 
        description=description,
        field=field
    )


@c4k.post('/api/levels/{level_id}', response_model=Result)
def solve_level(level_id, solution: Solution, session: SessionDep):
    level = session.get(Level, level_id)
    steps = play_code(level, solution.code)

    return Result(
        timestamp=time.time(),
        level_id=level_id,
        status='ok',
        steps=json.dumps(steps)
    )
