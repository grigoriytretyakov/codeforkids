import importlib.util
import os

from .constats import *

def chr2cell(char):
    return {
        '.': EMPTY,
        'h': HERO,
        'w': WALL,
        'b': BOX,
        'e': EXIT,
        's': SKELETON
    }[char]


def basic_level_check(game):
    field = game.field
    for line in field:
        for cell in line:
            if HERO in cell and EXIT in cell:
                return True

    return False


class Game:
    def __init__(self, level, commands):
        self.level = level
        self.commands = commands

        self.steps = []
        self.field = self.load_field()
        self.hero_line, self.hero_col = self.find_hero()
        self.hero_direction = UP

        level_checker_path = os.path.join(self.level.path, 'level_validation.py')
        print('level_checker_path:', level_checker_path)
        try:
            spec = importlib.util.spec_from_file_location('level_validation', level_checker_path)
            mod = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(mod)
            self.is_level_passed = mod.check_level
            print('from module')
        except (FileNotFoundError, ModuleNotFoundError, AttributeError) as e:
            self.is_level_passed = basic_level_check
            print('basic check', e)

    def load_field(self):
        field = []
        with open(os.path.join(self.level.path, 'map.txt')) as fp:
            for line in fp.readlines():
                line = line.strip()
                if not line:
                    continue

                field_line = []
                for c in line:
                    cell = chr2cell(c)
                    if cell != EMPTY:
                        field_line.append([cell])
                    else:
                        field_line.append([])
                field.append(field_line)
        return field

    def find_hero(self):
        for l_index, line in enumerate(self.field):
            for c_index, cell in enumerate(line):
                if cell and cell[0] == HERO:
                    return l_index, c_index
                
    def is_standing_on_skeleton(self):
        cell = self.field[self.hero_line][self.hero_col]
        return len(cell) >= 2 and cell[-2] == SKELETON
    
    def play(self):
        for cmd in self.commands:
            if cmd in (UP, DOWN, LEFT, RIGHT):
                self.hero_direction = cmd

            if self.can_do(cmd):
                self.do(cmd)

            if self.is_standing_on_skeleton():
                break

        if self.is_level_passed(self):
            self.steps.append([self.create_step_action('game', 'finish', status='win')])
        else:
            self.steps.append([self.create_step_action('game', 'finish', status='loose')])

    def calc_offset(self, cmd):
        directions = {
            LEFT: (0, -1),
            UP: (-1, 0),
            RIGHT: (0, 1),
            DOWN: (1, 0)
        }
        if cmd == JUMP:
            line, col = directions[self.hero_direction]
            line = line * 2
            col = col * 2
        elif cmd in directions:
            line, col = directions[cmd]
        else:
            return False
        
        return line, col
    
    def is_inside_bounds(self, line, col):
        return 0 <= line < len(self.field) and 0 <= col < len(self.field[0])
    
    def is_empty(self, line, col):
        return not self.field[line][col]

    def can_do(self, cmd):
        offfset = self.calc_offset(cmd)
        if not offfset:
            return False
        line, col = offfset
        
        next_line = self.hero_line + line
        next_col = self.hero_col + col

        if not self.is_inside_bounds(next_line, next_col):
            return False
        
        if self.is_empty(next_line, next_col):
            return True
        
        if self.field[next_line][next_col][-1] == WALL:
            return False
        
        if self.field[next_line][next_col][-1] == BOX and (
            not self.is_empty(next_line + line, next_col + col)
            and self.field[next_line + line][next_col + col][-1] != EXIT):
            return False
        
        return True

    def create_step_action(self, item_type, action, **kwargs):
        move = {
            'item': item_type,
            'action': action
        }

        if kwargs:
            for k, v in kwargs.items():
                move[k] = v

        return move

    def do(self, cmd):
        line, col = self.calc_offset(cmd)

        step = []

        next_line = self.hero_line + line
        next_col = self.hero_col + col
        if self.field[next_line][next_col] and self.field[next_line][next_col][-1] == BOX:
            self.field[next_line][next_col].pop()
            self.field[next_line + line][next_col + col].append(BOX)
            step.append(self.create_step_action(
                'box', 'move', src=(next_line, next_col), dst=(next_line + line, next_col + col)))
        
        step.append(self.create_step_action('hero', 'move', src=(self.hero_line, self.hero_col), dst=(next_line, next_col)))      
        self.field[self.hero_line][self.hero_col].pop()
        self.hero_line = next_line
        self.hero_col = next_col
        self.field[self.hero_line][self.hero_col].append(HERO)

        self.steps.append(step)




        


    