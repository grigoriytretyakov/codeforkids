# Возможные состояния ячеек
EMPTY = 0
HERO = 1
WALL = 2
BOX = 3
EXIT = 4
SKELETON = 5

# Команды
LEFT = 'left'
UP = 'up'
RIGHT = 'right'
DOWN = 'down'
JUMP = 'jump'