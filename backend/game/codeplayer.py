'''Модуль для формирования очереди команд, не запуская код непосредственно в 
пространстве игры. Такое разделение сделано, чтобы избежать модификации 
объектов игры через код ученика.
'''

from .game import Game

class HeroModel:
    def __init__(self) -> None:
        self.commands = []

    def up(self):
        self.commands.append('up')

    def down(self):
        self.commands.append('down')

    def right(self):
        self.commands.append('right')

    def left(self):
        self.commands.append('left')

    def jump(self):
        self.commands.append('jump')

    def dragon(self):
        # Это тоже прыжок (ученик попросил)
        self.jump()


def play_code(level_id, source):
    hero = HeroModel()
    exec(source, {'hero': hero})
    
    game = Game(level_id, hero.commands)
    game.play()

    return game.steps