Перепрыгни стенки.

Прыгать можно при помощи команды `hero.jump()` или `hero.dragon()`

И не забывай про циклы:

```
for _ in range(5):
    hero.jump()
    hero.up()
```
