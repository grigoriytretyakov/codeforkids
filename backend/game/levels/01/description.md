Это самый первый уровень игры. Тут мы начнём знакомиться с программированием и языком программирования Python.
Для этого тебе нужно будет управлять своим героем.

Программу для управления своим героем нужно будет писать в редакторе справа =>

В первой задаче нужно дать команду герою сдвинуться с места. Наш герой в программе называется `hero`. Управлять героем можно, вызывая его команды.

Попробуй переместить героя вправо при помощи команды.

```Python
hero.right()
```

А чтобы он сделал два шага, нужно дать герою две команды:


```Python
hero.right()
hero.right()
```

Чтобы пройти уровень, нужно сделать хотя бы один шаг.