def check_level(game):
    return set(game.commands) == set(['right', 'left', 'down', 'up'])