
from game.constats import *

def check_level(game):
    field = game.field
    for line in field:
        for cell in line:
            if HERO in cell and EXIT in cell:
                return not ('dragon' in game.commands or 'jump' in game.commands)

    return False
